class Products
{
  final int p_id,p_type,p_model,p_color,p_mfr,p_details,
      p_stock,p_brand,is_new,is_orginal;

  final String vin,p_commint,p_cdate,tag;

  Products({
    this.is_new, this.is_orginal, this.vin, this.tag,
    this.p_id,this.p_type, this.p_model,
    this.p_color,this.p_mfr , this.p_details,
    this.p_commint,this.p_brand,this.p_cdate,this.p_stock,
}
  );

  factory Products.fromMap(Map<String, dynamic> json)=> new Products(

    p_id: int.tryParse(json['p_id'].toString().trim()),
    p_type: int.tryParse(json['p_type'].toString().trim()),
    p_model: int.tryParse(json['p_model'].toString().trim()),
    p_color: int.tryParse(json['p_color'].toString().trim()),
    p_mfr: int.tryParse(json['p_mfr'].toString().trim()),
    p_details: int.tryParse(json['p_details'].toString().trim()),
    p_stock: int.tryParse(json['p_stock'].toString().trim()),
    p_brand: int.tryParse(json['p_brand'].toString().trim()),
    is_new: int.tryParse(json['is_new'].toString().trim()),
    is_orginal: int.tryParse(json['is_orginal'].toString().trim()),


    vin: json['vin']?? '',
    p_commint: json['p_commint']?? '',
    p_cdate: json['p_cdate']?? '',
    tag: json['tag']?? '',
  );

  Map<String,dynamic> toMap()=>{

    'p_id':p_id,
    'p_type':p_type,
    'p_model':p_model,
    'p_color':p_color,
    'p_mfr':p_mfr,
    'p_details':p_details,
    'p_stock':p_stock,
    'p_brand':p_brand,
    'is_new':is_new,
    'is_orginal':is_orginal,

    'vin':vin,
    'p_commint':p_commint,
    'p_cdate':p_cdate,
    'tag':tag,
  };


}