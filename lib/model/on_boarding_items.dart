import 'package:flutter/material.dart';

class OnBoardingItems
{
  String  _titles,_desc,_images;
  IconData _icons;

  OnBoardingItems(this._titles, this._desc, this._images, this._icons);

  IconData get icons => _icons;

  get images => _images;

  get desc => _desc;

  String get titles => _titles;
}