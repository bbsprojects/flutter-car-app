import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_flutter_app/fragments/home_page.dart';
import 'package:my_flutter_app/routes/page_routes.dart';
import 'package:my_flutter_app/widgets/drawer_body_item.dart';
import 'package:my_flutter_app/widgets/drawer_header.dart';

// refrence thirdrocktechkno.com/blog/how-to-implement-navigation-drawer-in-flutter
class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Drawer(
      child:Container(
        color: Theme.of(context).primaryColor,

      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          createDrawerHeader(),
          createDrawerBodyItem(
            icon:CupertinoIcons.car, text:'سيارات',
            onTap: (){
              Navigator.pushReplacementNamed(
                  context, PageRoutes.home);
            }

          ),
          createDrawerBodyItem(
              icon: Icons.search, text:'بحث عن سيارة',
                ),
              Divider(
              color: Colors.grey,
              ),
              createDrawerBodyItem(
              icon: Icons.account_circle,text:'بروفايل',
              onTap: (){Navigator.pushReplacementNamed(context, PageRoutes.profile);}

              ),

              createDrawerBodyItem(
              icon: Icons.notifications_active,text: 'ألإشعارات',
               onTap: (){Navigator.pushReplacementNamed(context, PageRoutes.notification);}
              ),
              Divider(
              color: Colors.grey,
              ),

              createDrawerBodyItem(
              icon:Icons.contact_mail, text:'تسجيل الدخول',
                  onTap: (){ Navigator.pushReplacementNamed(context, PageRoutes.login);}
              ),
              createDrawerBodyItem(
              icon: Icons.settings,text: 'الإعدادات'),
                Divider(
                color: Colors.grey

          ),

          // the others list for guides screen

          ListTile(
            title: Text('مساعدة'),
            onTap: (){},
          ),
          ListTile(
            title: Text('سياسة الخصوصية'),
            onTap: (){},
          ),
          ListTile(
            title: Text('شارك التطبيق '),
            onTap: (){},
          ),
          ListTile(
            title: Text(' حول التطبيق'),
            subtitle: Text('الإصدار 1.0.0'),
            onTap: () {},
          ),
        ],
      ),
      ),
    );
  }
}
