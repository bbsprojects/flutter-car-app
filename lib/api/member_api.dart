import 'package:http/http.dart' as http;
import 'package:my_flutter_app/model/members.dart';
import 'dart:convert';

import 'package:my_flutter_app/utils/api_path.dart';

class MemberApi {

  Future<Members> sendLoginInfo(Members member) async {
    String login_api = base_api + '';

    // set user with info from register screen
    Members myMember = member;
    var response = await http.post(login_api, body: {myMember.toMap()});

    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    // check status code
    Members member_item = Members();
    if (response.statusCode == 200) {
      var jsonData = jsonDecode(response.body);
      // check type of variable data
      print('data type:${jsonData.runtimeType}'); // must return json datatype
      // if there is name for list of json data
      // get only the array data:
      var data = jsonData['data'];
      print(data); // get all objects and info

      member_item = data;
      print(member_item.toString());
    }
    return member_item;
  }

  Future<String> sendRegestrationInfo(Members member) async {
    String regester_api = base_api + new_member_abi;
    String regester_opearation;

    // set user with info from register screen
    Members member1 = member;
    var response = await http.post(regester_api, headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    }, body: {
      member1.toMap()
    });
//json.encode()
    print('Response status: ${response.statusCode}');

    // chech status code
    if (response.statusCode == 200) {
      print('Response body: ${response.body}');
      var jsonData = jsonDecode(response.body);
      // check type of variable data
      print('data type:${jsonData.runtimeType}'); // must return json datatype
      // if there is name for list of json data
      // get only the array data:
      var data = jsonData['data'];
      print(data); // get all objects and info

      for (var item in data) {
        print(item.toString()); // get list of full name
        regester_opearation = item.toString();
      }
    }
    return regester_opearation;
  }
}
