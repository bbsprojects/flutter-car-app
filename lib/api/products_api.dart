import 'package:http/http.dart'as http;
import 'dart:convert';

import 'package:my_flutter_app/model/products.dart';
import 'package:my_flutter_app/utils/api_path.dart';

class ProductApi {
 Future <List <Products>> fetchAllProducts()
 async

  {
    List <Products> products_items =List<Products> ();
    String uri =base_api+'';
    var response = await http.get(uri);
    print('response status: ,${response.statusCode}');

  if(response.statusCode ==200)
    {
      var jsonData=jsonDecode(response.body);
      /*
      // one way :
      var products =jsonData['products'];

      for( var item in products)
        {
          Products product = Products(
              p_id :item['p_id'],
              p_type: item['p_type'],
              p_model: item['p_model'],
              p_color: item['p_color'],
              p_mfr :  item['p_mfr'],
              p_details: item['p_details'],
              p_stock: item['p_stock'],
              p_brand:  item['p_brand'],
              is_new : item['is_new'],
              is_orginal: item['is_orginal'],
              p_commint: item['p_commint'],
              p_cdate: item['p_cdate'],
              tag :  item['tag'],
              vin: item['vin']
          );
          */

      // other way
            var products =jsonData['products'];
            for(var item in products) {

              products_items.add( Products.fromMap(item));
            }

        }
  return products_items;

  }
}