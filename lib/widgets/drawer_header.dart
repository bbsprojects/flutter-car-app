import 'package:flutter/material.dart';

Widget createDrawerHeader() {
  return DrawerHeader(
      margin: EdgeInsets.zero,
      padding: EdgeInsets.zero,
      decoration: BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.fill,
              image:  ExactAssetImage('assets/images/bg_second_board.png')
          )
          ),

      child: Stack(children: <Widget>[
        Positioned(
            bottom: 12.0,
            left: 16.0,
            child: Text("اهلا بك في عالم السيارات ",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 25.0,
                    fontWeight: FontWeight.w500,
                    shadows: <Shadow>[
                Shadow(
                offset: Offset(5.0, 5.0),
              blurRadius: 3.0,
              //color: Color.fromARGB(255, 0, 0, 0),
                  color: Colors.deepOrange
            ),]
                ),
            ),
        ),
      ],
      ));
}