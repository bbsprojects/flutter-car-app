import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
class HarajView extends StatefulWidget {
  @override
  _HarajViewState createState() => _HarajViewState();
}

class _HarajViewState extends State<HarajView> {
  // to get different color to category
  List<Color> category_colors =[
    Colors.deepOrange.shade400,
    Colors.indigoAccent.shade400,
    Colors.deepOrange.shade400,
    Colors.blue.shade400,

  ];
  Random random =Random();
  Color _getRandomColor()
  {
    return category_colors[random.nextInt(category_colors.length)];
  }
  @override
  Widget build(BuildContext context) {

    return ListView.builder(
        itemBuilder: (context,position)
            {
              return Card(
                margin: EdgeInsets.only(top: 10,bottom: 10),
                child: Container(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    children: <Widget>[
                      _harajAuthorRow(),
                      _harajDetailItemRow(),
                      _drawDivider(),
                      _harajCartRow(),
                    ],
                  ),
                ),
              );
            },
      itemCount: 20,

    );
  }

  _harajAuthorRow() {

    TextStyle _Author_name =TextStyle(color: Colors.grey.shade700,
        fontWeight: FontWeight.bold);
    TextStyle _Author_desc=TextStyle(color:_getRandomColor(),
        fontWeight: FontWeight.w600);

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: ExactAssetImage('assets/images/bg_offer.png'),
                  fit: BoxFit.cover,
                ),
                shape: BoxShape.circle,
              ),
              height: 45,
              width: 45,
              margin:EdgeInsets.only(left:8.0,right: 5),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('بي بي سوفت',style: _Author_name,
                ),
                SizedBox(
                  height: 8,
                ),
                Row(
                  children: <Widget>[
                    Text('15 Min .',style:TextStyle(
                      color: Colors.grey.shade500,
                    ),),
                    Text('كاتجوري',style: _Author_desc,),

                  ],
                ),
              ],
            ),

          ],
        ),

        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            IconButton(icon:Icon(Icons.favorite,
            color: Colors.grey,),
                onPressed:(){
                }),
            IconButton(icon:Icon(Icons.comment,
              color: Colors.grey,),
                onPressed:(){
                }),
          ],
        ),

      ],
    );

  }

  _harajDetailItemRow() {
    // style of title and desc
    TextStyle detail_item_title =TextStyle(
      fontSize: 17,
      fontWeight: FontWeight.w500,
    );
    TextStyle detail_item_desc=TextStyle(
        color:Colors.grey,
        fontWeight: FontWeight.w700,
      fontSize: 15,

    );

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: ExactAssetImage('assets/images/bg_second_board.png'),
              fit: BoxFit.cover
            ),
          ),
          width: 130,
          height: 130,
          margin: EdgeInsets.all(8),
        ),
        Expanded(
          child: Column(
            children: <Widget>[
              Text('هونداي اكسنت 2010 فل اوبشن ',
                  style:detail_item_title ),
              SizedBox(height: 10,),
              Text('نبدة عن مواصفات السيارة و فل اوبشن دفع أمامي والكثير من المواصفات المتميزىة ',
              style: detail_item_desc,),
            ],
          ),
        ),
      ],
    );

  }

  _harajCartRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[

        Row(
          children: <Widget>[
            IconButton(icon:Icon(Icons.more,
              color: Colors.grey,),

                onPressed:(){
                }),
            Text('المزيد'),
          ],
        ),

        Row(
          children: <Widget>[
            Text('شراء'),


            IconButton(icon:Icon(Icons.shopping_cart,
              color: Colors.grey,),
              onPressed:(){
              },),
          ],
        ),
        Row(
          children: <Widget>[
            Text('السعر  '),
            Text('100000'),
            Text(' RY',),
          ],
        ),

      ],

    );
  }

  Widget _drawDivider() {
    return Container(
      margin: EdgeInsets.only(top: 10),
      height: 0.5,
      width: double.infinity,
      color: _getRandomColor(),
    );
  }
}
