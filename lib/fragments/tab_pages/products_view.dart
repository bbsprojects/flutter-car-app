import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_flutter_app/fragments/Details_pages/product_details.dart';
import 'package:my_flutter_app/model/products.dart';

class ProductsView extends StatefulWidget {
  @override
  _ProductsViewState createState() => _ProductsViewState();
}

class _ProductsViewState extends State<ProductsView> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _drawHeader(),
          _drawNewHyundai(),
          _drawMostRating(),
        ],
      ),
    );
  }

  // function to draw header with bg image
  Widget _drawHeader() {
    TextStyle _header_title = TextStyle(
        color: Colors.white, fontSize: 22, fontWeight: FontWeight.w600);
    TextStyle _header_desc = TextStyle(color: Colors.white70, fontSize: 17);

    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * .25,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: ExactAssetImage('assets/images/bg_second_board.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 38, left: 38),
              child: Text(
                'كل ما هو جديد بعالم السيارات ',
                style: _header_title,
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(right: 48, left: 48),
              child: Text(
                'اولا بأول ',
                style: _header_desc,
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }

  // this function to draw all news in the card
  Widget _drawNewHyundai() {
    return Container(
      color: Colors.grey.shade100,
      child: Column(
        //crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _drawSectionTitle('جديد هونداي'),
          ),
          Padding(
            padding: EdgeInsets.all(8),
            child: Card(
              child: Column(
                children: <Widget>[
                  _drawSingleRowNewHyundai(),
                  _drawDivider(),
                  _drawSingleRowNewHyundai(),
                  _drawDivider(),
                  _drawSingleRowNewHyundai(),
                  _drawDivider(),
                  _drawSingleRowNewHyundai(),
                  _drawDivider(),
                ],
              ),
            ),
          ),

          // the next section in product page for
        ],
      ),
    );
  }

  // function to draw the row in card for the news hyundai
  Widget _drawSingleRowNewHyundai() {
    return GestureDetector(
      onTap: () {
        // Navigator.pushReplacementNamed(context, ProductDetails.route_name);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return ProductDetails();
          }),
        );
      },
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Row(
          children: <Widget>[
            SizedBox(
              child: Image(
                image: ExactAssetImage('assets/images/bg_second_board.png'),
                fit: BoxFit.cover,
              ),
              width: 100,
              height: 100,
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'هونداي اكسنت سبورت 2020',
                    maxLines: 3,
                    style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('معرض اليمامة'),
                      Row(
                        children: <Widget>[
                          IconButton(
                              icon: Icon(
                                Icons.shopping_cart,
                                color: Theme.of(context).primaryColor,
                              ),
                              onPressed: () {}),
                          Text(
                            'شراء',
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                            ),
                          )
                        ],
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _drawMostRating() {
    return Padding(
      padding: EdgeInsets.all(8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(10),
            child: _drawSectionTitle('الأكثر تقييما '),
          ),
          _drawMostPopularCard(Theme.of(context).primaryColor, 'sport'),
          SizedBox(
            height: 15,
          ),
          _drawMostPopularCard(Theme.of(context).accentColor, 'box'),
          SizedBox(
            height: 15,
          ),
          _drawMostPopularCard(Theme.of(context).primaryColor, '4XE'),
          SizedBox(
            height: 50,
          )
        ],
      ),
    );
  }

  // to divider between rows
  Widget _drawDivider() {
    return Container(
      height: 0.7,
      width: double.infinity,
      color: Theme.of(context).accentColor,
    );
  }

  // to set the name of sections in the products page
  // like 'جديد هونداي', 'الفنيين ' etc...
  _drawSectionTitle(String title) {
    return Text(
      title,
      style: TextStyle(
          color: Theme.of(context).accentColor,
          fontWeight: FontWeight.w600,
          fontSize: 17),
    );
  }

  Widget _drawMostPopularCard(Color color, String category) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return ProductDetails();
          }),
        );
      },
      child: Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: ExactAssetImage('assets/images/bg_second_board.png'),
                  fit: BoxFit.cover,
                ),
              ),
              width: double.infinity,
              height: MediaQuery.of(context).size.height * .25,
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 16,
                left: 16,
              ),
              child: Container(
                padding:
                    EdgeInsets.only(left: 30, right: 30, top: 1, bottom: 1),
                decoration: BoxDecoration(
                  color: color,
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Text(
                  category,
                  style: TextStyle(
                      color: Colors.white70, fontWeight: FontWeight.w600),
                ),
              ),
            ),
            Padding(
              padding:
                  EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),
              child: Text(
                'زمعلومات عن السيارة ز.............. ',
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
              child: Row(
                children: <Widget>[
                  Icon(Icons.stars, color: color),
                  SizedBox(
                    width: 5,
                  ),
                  Text('حصلت على اعلى تقييما'),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
