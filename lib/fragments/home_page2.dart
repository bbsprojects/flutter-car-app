import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:my_flutter_app/api/member_api.dart';
import 'package:my_flutter_app/constants.dart';
import 'package:my_flutter_app/drawer/my_drawer.dart';
import 'package:my_flutter_app/fragments/pop_down_pages/about.dart';
import 'package:my_flutter_app/fragments/pop_down_pages/help.dart';
import 'package:my_flutter_app/fragments/recommendedcars.dart';
import 'package:my_flutter_app/model/members.dart';
import 'package:my_flutter_app/routes/page_routes.dart';

class HomePage extends StatefulWidget {
  static const String route_name = '/homePage2';

  @override
  _HomePageState createState() => _HomePageState();
}

enum PopOutMenu { HELP, ABOUT, CONTACT, SETTINGS }

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  int _selectedIndex;
  List<IconData> listicn = [
    FontAwesomeIcons.car,
    FontAwesomeIcons.productHunt,
    FontAwesomeIcons.warehouse,
    FontAwesomeIcons.fontAwesomeFlag,
    FontAwesomeIcons.carCrash,
    Icons.local_offer,
  ];
  List<String> listCatgoryname = [
    "سيارات",
    "قطع غيار",
    "حراج",
    "مستلزمات سيارات",
    "فنيين",
    "عروض",
  ];

  Widget _buildIcon(int index) {
    return GestureDetector(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Stack(
          alignment: Alignment.topCenter,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.all(10),
              width: 100,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(10)),
              child: Column(
                children: <Widget>[
                  Container(
                    height: 60,
                    width: 60,
                    decoration: BoxDecoration(
                      color: _selectedIndex == index
                          ? kSecondaryColor
                          : kSecondaryColor,
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    child: Icon(
                      listicn[index],
                      size: 25,
                      color:
                          _selectedIndex == index ? kPrimaryColor : Colors.grey,
                    ),
                  ),
                  Text(
                    '${listCatgoryname[index]}',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: _selectedIndex == index
                            ? kPrimaryColor
                            : kTextColor),
                  )
                ],
              ),
            ),
            _selectedIndex == index
                ? Positioned(
                    bottom: 0,
                    child: Container(
                      width: 90,
                      height: 3,
                      decoration: BoxDecoration(
                        color: kPrimaryColor,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
      ),
      onTap: () {
        setState(() {
          _selectedIndex = index;
        });
        print('_selectedIndex $_selectedIndex');
      },
    );
  }

  Widget _buildHragcard() {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            GestureDetector(
              child: Text('حراج سيارات'),
              onTap: () {},
            ),
            GestureDetector(child: Text('حراج قطع غيار'), onTap: () {})
          ],
        ),
      ],
    );
  }

  Widget _buildCard() {
    if (_selectedIndex != null) {
      if (_selectedIndex == 2) {
        return _buildHragcard();
      } else
        return Container();
    } else
      return Container();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: kSecondaryColor,
      appBar: AppBar(
        title: Text(
          'سيارات',
          style: TextStyle(fontSize: 20),
        ),
        backgroundColor: kPrimaryColor,
        centerTitle: false,
        actions: <Widget>[
          IconButton(icon: Icon(Icons.search), onPressed: () {}),
          _popOutMenu(context),
        ],
        // set the tabs
      ),
      drawer: MyDrawer(),
      body: SafeArea(
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 120, right: 20),
              child: Text('سيارات'),
            ),
            SizedBox(
              height: 20.0,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: listicn
                    .asMap()
                    .entries
                    .map((MapEntry map) => _buildIcon(map.key))
                    .toList(),
              ),
            ),
            _buildCard(),
            SizedBox(
              height: 20,
            ),
            RecommendedCars(),
          ],
        ),
      ),
    );
  }

  _popOutMenu(BuildContext context) {
    return PopupMenuButton<PopOutMenu>(
      itemBuilder: (context) {
        return [
          PopupMenuItem<PopOutMenu>(
            value: PopOutMenu.ABOUT,
            child: Text('ABOUT'),
          ),
          PopupMenuItem<PopOutMenu>(
            value: PopOutMenu.HELP,
            child: Text('HELP'),
          ),
          PopupMenuItem<PopOutMenu>(
            value: PopOutMenu.CONTACT,
            child: Text('CONTACT'),
          ),
          PopupMenuItem<PopOutMenu>(
            value: PopOutMenu.SETTINGS,
            child: Text('SETTINGS'),
          ),
        ];
      },
      onSelected: (PopOutMenu menu) {
        switch (menu) {
          case PopOutMenu.ABOUT:
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) {
                return AboutPage();
              }),
            );
            break;
          case PopOutMenu.HELP:
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) {
                return HelpPage();
              }),
            );
            break;
          case PopOutMenu.CONTACT:
            break;
          case PopOutMenu.SETTINGS:
            Navigator.pushReplacementNamed(context, PageRoutes.setting);
            break;
        }
        //TODO ;
      },
      icon: Icon(Icons.more_vert),
    );
  }
}
