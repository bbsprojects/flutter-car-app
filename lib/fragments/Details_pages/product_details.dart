

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:parallax_image/parallax_image.dart';

import '../../constants.dart';

class ProductDetails extends StatefulWidget  {
  static const String route_name='/ProductDetailsPage';

  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            actions: <Widget>[
              Icon(Icons.search)
            ],
            expandedHeight:200 ,
            floating:true,
            pinned: true,
            centerTitle: true,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              title: Text('هونداي أكسنت سبورت 2020',style: TextStyle(
                fontSize: 17,
                color: Colors.white,
                fontWeight: FontWeight.w500
              ),),
              background: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: ExactAssetImage('assets/images/bg_second_board.png')
                    ,fit: BoxFit.cover,
                  ),
                ),
              ),
            ),

          ),
          SliverList(
          delegate: SliverChildBuilderDelegate(
              (context,position){

                if(position == 0 )
                  {
                    return _drawProductDetails();
                  }
                else if (position ==1)
                {
                  return _drawMainDetails();
                }
                else if (position ==2)
                {
                  return _drawMoreDetails();

                }
                else if (position ==3)
                {
                  return _drawMoreDetailsContent();

                }

                else if (position ==4)
                {
                  return _drawMoreImages();

                }

                else
                  {
                    return _drawFooterProdectDetails();
                  }
                },
            childCount: 6,
          ),
            ),
        ],
      ),
    );
  }

  Widget _drawProductDetails() {
    return Card(
      shadowColor: Colors.white70,
      color: Colors.grey.shade300,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(CarDetails),
          Text('120000 ريال'),
          IconButton(icon: Icon(Icons.favorite),
            onPressed: (){},
            //color:_is_selected ?true? Colors.grey.shade500,
            color: Colors.grey.shade500,
          ),
        ],
      ),
    );
  }

  Widget _drawMainDetails() {

    return Card(
      child:Padding(
        padding: const EdgeInsets.only(top:15.0,bottom: 25,),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(first_dt),
                SizedBox(
                  width: 40,
                ),
                Text('toyota'),
              ],
            ), Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(second_dt),
                SizedBox(
                  width: 40,
                ),
                Text('اكسنت'),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(third_dt),
                SizedBox(
                  width: 40,
                ),
                Text('2020'),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(forth_dt),
                SizedBox(
                  width: 40,
                ),
                Text('جديدة'),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(fifth_dt),
                SizedBox(
                  width: 40,
                ),
                Text('أبيض'),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(sixth_dt),
                SizedBox(
                  width: 40,
                ),
                Text('فل ابشن'),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(sev_dt),
                SizedBox(
                  width: 40,
                ),
                Text('0 كم'),
              ],
            ),


          ],
        ),
      ),
    );

  }

  Widget _drawFooterProdectDetails() {
    return Container(
      child: Text('footer'),
    );

  }

  Widget _drawMoreDetails() {
    return Card(
      shadowColor: Colors.white70,
      color: Colors.grey.shade300,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(CarMoreDetails),
      ),
    );
  }

  Widget _drawMoreDetailsContent() {
    return Card(
      child: Padding(
        padding: const EdgeInsets.only(top:15.0,bottom: 25,),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(eigh_dt),
                SizedBox(
                  width: 40,
                ),
                Text('toyota'),
              ],
            ), Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(second_dt),
                SizedBox(
                  width: 40,
                ),
                Text('اكسنت'),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(nin_dt),
                SizedBox(
                  width: 40,
                ),
                Text('2020'),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(tn_dt),
                SizedBox(
                  width: 40,
                ),
                Text('جديدة'),
              ],
            ),

          ],
        ),
      ),
    );
  }

  Widget _drawMoreImages() {


    return new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
    new Container(
    padding: const EdgeInsets.all(20.0),
    child: new Text(
    'مزيد من الصور',
    //style: textTheme.title,
    ),
    ),
    new Container(
    padding: const EdgeInsets.symmetric(vertical: 10.0),
    constraints: const BoxConstraints(maxHeight: 200.0),
    child: new ListView.builder(
    scrollDirection: Axis.horizontal,
    itemBuilder: _buildHorizontalChild,
    ),
    ),
    ],
    );

}



  Widget _buildHorizontalChild(BuildContext context, int index) {

    index++;
    if (index > 7) return null;
    return new Padding(
      padding: const EdgeInsets.only(right: 10.0),
      child: new ParallaxImage(
        extent: 100.0,
       /* image: new ExactAssetImage(
          'images/img$index.jpg',
        ),*/
       image: ExactAssetImage('assets/images/bg_second_board.png'),
      ),
    );
  }
  }

